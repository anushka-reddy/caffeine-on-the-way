# Caffeine on the Way

This repository contains the source code for the "Caffeine on the Way" website, a modern and responsive website designed for a local coffee shop. It offers an engaging online presence with an online menu, shop information, a location map, and a contact form for customer interactions.

## Features

- **Responsive Design**: Optimized for various devices to ensure a seamless user experience.
- **Online Menu**: Features a comprehensive look at the coffee shop's offerings, complete with images, descriptions, and prices.
- **Category Filtering**: Allows users to filter menu items by categories like lattes and mochas.
- **Carousel**: An interactive carousel in the menu section for a dynamic presentation of items.
- **About Section**: Provides insights into the coffee shop's values and community engagement.
- **Location Map**: Integrated Google Maps to help locate the shop easily.
- **Contact Form**: A modal window form for customers to make inquiries or provide feedback.
- **Social Media Links**: Connects customers to the shop's social media for updates and community building.

## Technologies Used

- HTML5
- CSS3
- JavaScript
- jQuery
- Bootstrap
- Owl Carousel
- AOS (Animate on Scroll) Library
- Google Maps API

## Getting Started

To run the website locally:

1. Clone the repository:
```bash
git clone https://github.com/your-repo/caffeine-on-the-way.git
```
2. Open `index.html` in a web browser.

## Contributing

Contributions are welcome! Please feel free to submit a pull request or open an issue for any suggestions or improvements.

## Credits

- Coffee images and icons used in this project are sourced from various free stock image websites and are licensed for commercial use.
- The video used in the "Feel our coffee" section is embedded from YouTube, with all credits belonging to the original content creator.

**Note**: The website does not include any server-side processing or database integration for the contact form submissions.