// Ensure DOM is fully loaded before executing the script
$(document).ready(function() {
    try {
        // Initialize Animate On Scroll (AOS) library for scroll-triggered animations
        AOS.init();
    } catch (error) {
        // Log any errors encountered during AOS initialization
        console.error("Error initializing AOS:", error);
    }

    // Clone items within '#menu-carousel .item' to not affect the original items during manipulation
    var originalItems = safeClone('#menu-carousel .item');
    
    // Initialize Owl Carousel with the cloned items, wrapping in a function to handle potential errors
    var owl = safeInitializeCarouselWithItems(originalItems);

    // Enhance carousel control with keyboard for accessibility: Right arrow moves to next item, left arrow to previous
    owl.on('keydown', function(e) {
        if (e.key === "ArrowRight") {
            owl.trigger('next.owl.carousel');
        } else if (e.key === "ArrowLeft") {
            owl.trigger('prev.owl.carousel');
        }
    });

    // Filter functionality: On clicking a category filter, filter the carousel items based on data attributes
    $('.category-filter').on('click', function() {
        try {
            // Get filter type from clicked element's data-filter attribute, convert to lowercase to ensure case-insensitive matching
            var filter = $(this).attr('data-filter').toLowerCase();
            // Filter original items for those that match the selected category or show all if 'all' is selected
            var filteredItems = filter === 'all' ? originalItems : originalItems.filter(function() {
                return $(this).data('category').toLowerCase() === filter;
            });

            // Reinitialize the carousel with the filtered items
            safeRebuildCarousel(owl, filteredItems);
        } catch (error) {
            // Log any errors that occur during the filtering process
            console.error("Error handling category filter click:", error);
        }
    });

    // Implement smooth scrolling for all anchor links not linking to "#" or "#0", improving user experience
    $('a[href*="#"]').not('[href="#"]').not('[href="#0"]').click(function(event) {
        safeSmoothScroll(event);
    });

    // Initialize accordion functionality, providing an interactive way to display content in a compact form
    safeSetupAccordion();
});

// Function to clone items safely, catching and logging any errors
function safeClone(selector) {
    try {
        // Clone selected items and return them
        return $(selector).clone();
    } catch (error) {
        // Log cloning errors and return an empty jQuery object as a fallback
        console.error("Error cloning items:", error);
        return $(); // Return empty jQuery object on failure
    }
}

// Function to initialize Owl Carousel with specific items, handling errors gracefully
function safeInitializeCarouselWithItems(items) {
    try {
        // Empty the carousel container and append new items before initializing Owl Carousel
        $('#menu-carousel').empty().append(items);
        // Initialize Owl Carousel with specific settings for responsiveness and return the instance
        return $('#menu-carousel').owlCarousel({
            loop: false,
            margin: 10,
            responsive: {
                0: { 
                    items: 1 
                },
                600: { 
                    items: 2 
                },
                1000: {
                    items: 3 
                }
            }
        });
    } catch (error) {
        // Log any errors that occur during carousel initialization
        console.error("Error initializing carousel with items:", error);
    }
}

// Function to rebuild the Owl Carousel with a new set of items, encapsulating error handling
function safeRebuildCarousel(owlInstance, items) {
    try {
        // Destroy the current instance of the carousel
        owlInstance.trigger('destroy.owl.carousel');
        // Reinitialize the carousel with the new set of items
        safeInitializeCarouselWithItems(items);
    } catch (error) {
        // Log any errors encountered during the carousel rebuild process
        console.error("Error rebuilding carousel:", error);
    }
}

// Function to implement smooth scrolling behavior for anchor links, with error handling
function safeSmoothScroll(event) {
    try {
        // Ensure the link is for the current page but not a generic hash link
        if (
            location.pathname.replace(/^\//, '') == event.currentTarget.pathname.replace(/^\//, '') &&
            location.hostname == event.currentTarget.hostname
        ) {
            // Attempt to find the target element based on the hash in the link
            var target = $(event.currentTarget.hash);
            target = target.length ? target : $('[name=' + event.currentTarget.hash.slice(1) + ']');
            // If a target is found, prevent default link behavior and smoothly
            // Check if the target exists to prevent default scrolling behavior
            if (target.length) {
                // Prevent the default anchor click behavior
                event.preventDefault();
                // Animate the scroll to the target element's position with a 1000ms duration
                $('html, body').animate({
                    scrollTop: target.offset().top
                }, 1000, function() {
                    // Once the scroll is complete, attempt to set focus to the target element
                    target.focus();
                    // If the target cannot take focus (e.g., not focusable), temporarily make it focusable
                    if (!target.is(":focus")) {
                        target.attr('tabindex','-1'); // Make element temporarily focusable
                        target.focus(); // Set focus again
                    }
                });
            }
        }
        } catch (error) {
            console.error("Error during smooth scroll:", error);
        }
    }

function safeSetupAccordion() {
    try {
        // Open the first section of the accordion by default to show some content immediately
        $(".accordion > li:first-child a").addClass("active").next().slideDown();

        // Add click event listeners to all accordion headers
        $(".accordion a").click(function(event) {
            // Prevent the default action to ensure the page doesn't jump to the anchor link
            event.preventDefault();
            var $this = $(this); // The clicked accordion header
            var $li = $this.closest("li"); // The list item containing the clicked header
            var $p = $li.find("p"); // The paragraph element that needs to be shown or hidden

            // Slide up all paragraph elements that are not the clicked element's sibling
            $(".accordion li p").not($p).slideUp();

            // Toggle the active class on the clicked header and slide its sibling paragraph up or down
            if ($this.hasClass("active")) {
                // If the header was already active, remove the class and slide its content up
                $p.slideUp();
                $this.removeClass("active");
            } else {
                // If the header was not active, make it active and slide down its content
                // Also, ensure that previously active headers are deactivated
                $(".accordion a.active").removeClass("active");
                $this.addClass("active");
                $p.slideDown();
            }
        });
        } catch (error) {
            console.error("Error setting up accordion:", error);
        }
    }